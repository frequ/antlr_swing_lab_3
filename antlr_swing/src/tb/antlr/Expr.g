grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat)+ EOF!;

stat
    : expr NL -> expr

    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
    | ID PODST expr NL -> ^(PODST ID expr)
    | forLoop NL -> forLoop
    | whileLoop NL -> whileLoop
    | doWhileLoop NL -> doWhileLoop
    | conditionalStatement NL -> conditionalStatement
    | PRINT expr NL -> ^(PRINT expr)

    | NL ->
    ;

expr
    : arithmeticExpr
      ( EQUAL^ arithmeticExpr
      | NOT_EQUAL^ arithmeticExpr
      )*
      ;
      
arithmeticExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR 
    : 'var'
    ;

conditionalStatement
    : IF^ LP! expr RP! THEN! (expr) (ELSE! (expr))?
    ;

forLoop
    : FOR^ expr DO! expr
    ;
    
whileLoop
    : WHILE^ expr DO! expr
    ;
    
doWhileLoop
    : DO^ expr WHILE! expr
    ;
    
LP
    : '('
    ;
    
RP
    : ')'
    ;
    
IF
    : 'if'
    ;
    
THEN
    : 'then'
    ;
    
PRINT
    : 'print'
    ;
    
ELSE
    : 'else'
    ;
    
WHILE
    : 'while'
    ;
    
DO
    : 'do'
    ;
    
FOR
    : 'for'
    ;
    
EQUAL
    : '=='
    ;
    
NOT_EQUAL
    : '!=='
    ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
