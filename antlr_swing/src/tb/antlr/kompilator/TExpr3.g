tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer number = 0;
}
prog  : (e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> decl(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st}, p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> substract(p1={$e1.st}, p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st}, p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st}, p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> assign(symbol={$ID.text}, value={$e2.st})
        | ID                       -> id(symbol={$ID.text})
        | INT                      -> int(i={$INT.text})
        | ^(EQUAL e1=expr e2=expr) -> equals(p1={$e1.st}, p2={$e2.st})
        | ^(NOT_EQUAL e1=expr e2=expr)              -> notEquals(p1={$e1.st}, p2={$e2.st})
        | ^(DO    e1=expr e2=expr) {number++;}       -> do(exp1={$e1.st}, exp2={$e2.st}, n={number.toString()})
        | ^(WHILE e1=expr e2=expr) {number++;}       -> while(exp1={$e1.st}, exp2={$e2.st}, n={number.toString()})
        | ^(FOR e1=expr e2=expr) {number++;}         -> for(exp1={$e1.st}, exp2={$e2.st}, n={number.toString()})
        | ^(IF e1=expr e2=expr e3=expr?) {number++;} -> ifStat(ifExpr={$e1.st}, thenExpr={$e2.st}, elseExpr={$e3.st}, n={number.toString()})
    ;
    